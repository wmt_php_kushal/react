import React from 'react';
// import logo from './logo.svg';
import './App.css';
// import Greet from './components/Greet';
// import Welcome from './components/Welcome';
// // import Message from './components/Message';
// import Counter from './components/Counter';
// import FunctionClick from './components/FunctionClick';
// import ClassClick from './components/ClassClick';
// import EventBind from './components/EventBind';
// import UserGreeting from './components/UserGreeting'
// import Toggle from './components/Toggle';
// import ParentComponent from './components/ParentComponent';
// import ReactForm from './components/ReactForm';
import Card from './components/Card';
// import Login from './components/Login';
// import Screen from './components/Screen';
// import Comment from './components/Comment';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        {/* <Screen></Screen> */}
        {/* <Login/> */}
        {/* <Comment></Comment> */}
        {/* <ReactForm></ReactForm> */}
        <Card></Card>
        {/* <img src={logo} className="App-logo" alt="logo" /> */}
        {/* <Message></Message> */}
        {/* <p>-------------------</p>
        <Counter></Counter>
        <p>-------------------</p> */}
        
        {/* <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>*/}
        {/* <Greet>Helloz...</Greet>
        <FunctionClick>CLICK</FunctionClick> */}
        {/*
        <Welcome>hello</Welcome>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a> */}
        {/* <p>-------------------</p>
        <UserGreeting></UserGreeting>
        <p>-------------------</p>
        <EventBind/>
        <p>-------------------</p>
        <Toggle/>
        <p>-------------------</p>
       <ClassClick/>
       <p>-------------------</p>
       <ParentComponent/>
       <p>-------------------</p> */}
      </header>
    </div>
  );
}

export default App;
