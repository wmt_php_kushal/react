import React, { Component } from 'react'
import Login from './Login'
import ReactForm from './ReactForm'

export class Screen extends Component {
    constructor(props) {
        super(props)
        this.state={
            isLogin:false,
            isSignUp:false

    
        }
        this.logInclickHandler=(event)=>{
            this.setState(state => ({
                    isLogin: !state.isLogin,
                    isSignUp: false
                
              }));       
            }  
            this.signUpclickHandler=(event)=>{
            this.setState(state => ({
                    isSignUp: !state.isSignUp,
                    isLogin: false
                
              }));       
            }     
            
            
    }
    
    // 
   

    render() {
        if(this.state.isLogin){
            return <Login/>
        }else if(this.state.isSignUp){
            return <ReactForm/>
        }
        return (
            <div>
                {/* <button onClick={(e)=>this.clickHandler(e)}></button> */}
                <button onClick={(e)=>this.logInclickHandler(e)} name='login'>Login</button>
                <button onClick={(e)=>this.signUpclickHandler(e)} name='signup'>Signup</button>
            </div>
        )
    }
}

export default Screen
