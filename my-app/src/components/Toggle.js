import React, { Component } from 'react'

class Toggle extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isToggleOn:"ON"
        };
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick(){
        this.setState(state => ({
            isToggleOn: !state.isToggleOn
          }));        console.log(this.state.isToggleOn)

        }      
    render() {
        return (
            <div>
    <button onClick={this.handleClick}>{this.state.isToggleOn ? 'Toggle:ON':'Toggle:OFF'}</button>
            </div>
        );
    }
}

export default Toggle
