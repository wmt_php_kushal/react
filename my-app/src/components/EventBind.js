import React, { Component } from 'react'

export class EventBind extends Component {
    constructor(props) {
        super(props)
        this.state = {
             message:'heyy'
        }
    }
    clickHandler=(e)=> {
        e.preventDefault();
        if(this.state.message==='heyy')
           this.setState(state => ({
           message:"Bye"
        }))
        else
        this.setState(state => ({
           message:'heyy'
        }));        
        console.log(this)
    }
    render() {
        const {message}=this.state
        return (
            <div>
                <span>This is exapmle of Event</span>
    <h1>{message}</h1>
                {/* <button onClick={()=>this.clickHandler()}>Event Click</button> */}
                <p><button onClick={this.clickHandler}>{this.state.message==='heyy' ? 'Change To: Bye':'Change To: heyy'}</button> 
                </p>
            </div>
        )
    }
}

export default EventBind