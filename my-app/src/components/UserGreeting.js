import React, { Component } from 'react'

class UserGreeting extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             isLoggedIn:true
            //  isLoggedIn:false
        }
    }
    
    render() 
    {
        //short circuit approach
        return(this.state.isLoggedIn && <div>Welcome Member</div> )
        //ternary approach
        // return(this.state.isLoggedIn ? <div>Welcome Member</div>:<div>Welcome guest</div>)
        //if else approach
        // let msg
        // if(this.state.isLoggedIn){
        //     msg=   <div>Welcome Member</div> 
        // }else{
        //     msg=   <div>Welcome guest</div>             
        // }
        // return(
        //     <div>
        //         {msg}
        //     </div>
        // )
    }
}

export default UserGreeting
