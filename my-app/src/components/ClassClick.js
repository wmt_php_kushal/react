import React, { Component } from 'react'

class ClassClick extends Component {
    clickHandler() {
        console.log('Clickedd')
    }
    render() {
        return (
            <div>
                <button onClick={this.clickHandler}>CLICKS</button>
            </div>
        )
    }
}

export default ClassClick
