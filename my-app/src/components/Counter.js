import React, { Component } from 'react'

export class Counter extends Component {
    constructor(props){
        super(props)
        this.state={
            count:0
        }
        
    }
    componentDidMount() {
        
        console.log('didMont')
    }
    componentWillUpdate(){
        console.log('willUpdate')
    }
    componentDidUpdate(){
        console.log('didUpdate')
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
        console.log('willUnmount');  
    }
    increment(){
        // this.state.count=this.state.count+1;
        // this.setState({
        //     count:this.state.count + 1},()=>{console.log('callback value:',this.state.count)});
        this.setState((prevState,props)=>({
            count:prevState.count+1
        }))
        console.log(this.state.count)
    }
    incrementFive(){
        this.increment()
        this.increment()
        this.increment()
        this.increment()
        this.increment()
    }
    render() {
        return (
            <div>
                Count: {this.state.count} 
            <button onClick={()=>this.incrementFive()}>
                Increment
            </button>
            </div>
        )
    }
}

export default Counter