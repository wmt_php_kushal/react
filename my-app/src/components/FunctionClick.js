import React from 'react'

function FunctionClick() {
    function clickHandler() {
        console.log('Clickedd')
    }
    return (   
        <div>
            <button onClick={clickHandler}>CLICK</button>
        </div>
    )
}

export default FunctionClick