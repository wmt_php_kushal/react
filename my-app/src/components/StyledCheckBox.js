import styled from 'styled-components'
import Icon from './Icon'

const StyledCheckbox=styled.div`
div:inline-block;
width: 16px;
  height: 16px;
  background: ${props => props.checked ? 'salmon' : 'papayawhip'};
  border-radius: 3px;
  transition: all 150ms;
  ${Icon} {
    visibility: ${props => props.checked ? 'visible' : 'hidden'}
  }

`
export default StyledCheckbox