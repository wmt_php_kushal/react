import React from 'react'
// import styled from 'styled-components'
// import HiddenCheckbox from './HiddenCheckbox'
// import StyledCheckbox from './StyledCheckBox'
// import Icon from './Icon'



// const CheckboxContainer = styled.div`
// display:inline-block;
// vertical-align:middle;
// `
// const Checkbox=({className,checked,...props})=>(
//   <CheckboxContainer className={className}>
//     <HiddenCheckbox checked={checked}{...props}/>
//     <StyledCheckbox checked={checked}>
//     <Icon viewBox="0 0 24 24">
//         <polyline points="20 6 9 17 4 12" />
//       </Icon>
//     </StyledCheckbox>
//   </CheckboxContainer>
// )

const Checkbox = props => (
    <input type="checkbox" {...props} />
  )
  
  export default Checkbox