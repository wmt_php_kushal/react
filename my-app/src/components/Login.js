import React, { Component } from 'react'
import ReactDom from 'react-dom'
import { renderIntoDocument } from 'react-dom/test-utils';

class Login extends Component {
    constructor(props) 
    {
        super(props)
        this.state = {
            email:'',
            password:'',               
            errorEmail:'',
            errorPassword:'',
        };
        this.handleChange=(event)=>{
            const{name,value}=event.target
            this.setState({
                [name]:value
            })
        }
    }
    handleSubmit =(event) =>{
        event.preventDefault();
        const valid=true;
        if(valid){
            this.setState(this.state)

                if(!((this.state.email)==='some@m.com')&&(!(this.state.password==='some@123'))){
                    console.log('wrong')
                    document.getElementById('main').innerHTML=`Hello Stranger`
                    // render(<span>Hello</span>,document.getElementById('showresult'))
                }else{
                    console.log('done')
                    document.getElementById('main').innerHTML=`Hello ${this.state.email}`
            }
            
        }
        }

    render() {
        const{email,password}=this.state
        
        return (
            <div>
                <div id='showresult'></div>
                <div id='main'>
                <form onSubmit={this.handleSubmit} >
                        <table >
                            <tbody>
                                <tr>
                                    <td>
                                    <label>Email:</label>
                                    </td>
                                    <td>
                                    <input name='email' type="text" value={email} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorEmail}</small>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label>Password:</label>
                                    </td>
                                    <td>
                                    <input name='password' type="password" value={password} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorPassword}</small>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td colSpan='2'><input type="submit" value="Submit"/></td>
                                </tr>
                            </tbody>
                        </table>
                </form>
            </div>
            <div>
            </div>
        </div>
        )
    }
}
export default Login
