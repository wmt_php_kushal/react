import React, { Component } from 'react'
// import Checkbox from './Checkbox';

class ReactForm extends Component {

    constructor(props) {
        super(props)

        this.state = {
            id_no:'',
            name: '',
            phone:'',
            note:'',
            gender:'',
            email:'',
            password:'',
            cnf_password:'',
            opt:'',
            sports1:'',
            sports2:'',
            sports3:'',
            checked:'',
            errorId_no:'',
            errorName:'',
            errorPhone:'',
            errorGender:'',
            errorEmail:'',
            errorChecked: false,
            errorPassword:'',
            errorNote:'',
            errorSports:'',
            errorOpt:''


        };
        this.handleChange=(event)=>{
            const{name,value}=event.target
            this.setState({
                [name]:value
            })
        }
        // this.changeId_No=(event)=>{
        //     this.setState({id_no:event.target.value})
        // }
        // this.changeName=(event)=>{
        //     this.setState({name:event.target.value})
        // }
        // this.changePhone=(event)=>{
        //     this.setState({phone:event.target.value})
        // }
        // this.changeNote=(event)=>{
        //     this.setState({note:event.target.value})
        // }
        // this.changeGender=(event)=>{
        //     this.setState({gender:event.target.value})
        // }
        // this.changeEmail=(event)=>{
        //     this.setState({email:event.target.value})
        // }
        // this.changePassword=(event)=>{
        //     this.setState({password:event.target.value})
        // }
        // this.changeCnf_Password=(event)=>{
        //     this.setState({cnf_password:event.target.value})
        // }
        // this.changeOpt=(event)=>{
        //     this.setState({opt:event.target.value})
        // }
        // this.handleCheckboxChange=(event)=>{
        //     console.log(event.target.value)
        //     this.setState({ 
        //         [sports1]: event.target.value
                
        //      })}
    // this.setState({ sports: this.state.checked })}
    }
    
    validator=()=>{
        let errorId_no='';
        let errorName='';
        let errorPhone='';
        let errorNote='';
        let errorEmail='';
        let errorPassword='';
        let errorCnf_Password='';
        let errorGender='';
        let errorOpt='';
        let errorSports=''
        if(!this.state.id_no){
            errorId_no='Id_No is empty Please fill it.';
        }
        if(!this.state.name){
            errorName='Name field is empty please fill it'
        }
        if(!this.state.phone){
            errorPhone='Phone field is empty please fill it'
        }else{
            if(!(this.state.phone).match(/^[0][1-9]\d{9}$|^[1-9]\d{9}$/)) {
                errorPhone='Please enter valid phone number'
            }
        }
        if(!this.state.note){
            errorNote='Note field is empty please fill it'
        }
        if(!this.state.email){
            errorEmail='Email field is empty please fill it'
        }else{
            if(!(this.state.email).match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)){
                errorEmail='Please enter the valid email'
            }
        }
        if(!this.state.password){
            errorPassword='Password field is empty please fill it'
        }else{
            if(!(this.state.password).match(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/i)){
                errorPassword='Please enter the valid password(Minimum eight characters, at least one letter, one number and one special character)'
            }
        }if(!this.state.cnf_password){
            errorCnf_Password='Confirm Password field is empty please fill it'
        }else{
            if(!(this.state.cnf_password===this.state.password)){
                errorCnf_Password='Please enter the same password to confirm'
            }
        }
        if(!this.state.gender){
            errorGender='please select your Gender'
        }
        // if((this.state.sports1==='')){
        //     console.log(this.state.sports1)
        //     errorSports='please select your checkbox'
        // }
        // if((this.state.sports2==='')){
        //     console.log(this.state.sports2)
        //     errorSports='please select your checkbox'
        // }
        // if((this.state.sports3==='')){
        //     console.log(this.state.sports3)
        //     errorSports='please select your checkbox'
        // }
        // if(!this.state.opt){
        //     errorOpt='please select any one option'
        // }
        if(errorId_no||errorName||errorNote||errorPhone||errorOpt||errorGender||errorEmail||errorPassword||errorCnf_Password||errorSports){
            this.setState({
                errorId_no,errorName,errorNote,errorPhone,errorOpt,errorGender,errorEmail,errorPassword,errorCnf_Password,errorSports
            });
            return false
        }   
        return true        
    }
    handleSubmit =(event) =>{
        event.preventDefault();
        const valid=this.validator();
        if(valid){
            this.setState(this.state)
            document.getElementById('main').innerHTML=`Hello ${this.state.email}`
            // alert(`ID_no: ${this.state.id_no},Name: ${this.state.name},Note: ${this.state.note},Phone: ${this.state.phone},Gender: ${this.state.gender},Email: ${this.state.email},Password: ${this.state.password},Opt: ${this.state.opt},Sport: ${this.state.sports1} ${this.state.sports2} ${this.state.sports3} `);
        }
        
        }
        
    
    render() {
        const{id_no,name,note,opt,phone,email,password,cnf_password}=this.state
        return (
            <div>
                <div id='main'>
                    <form onSubmit={this.handleSubmit}>
                        <table >
                            <tbody>
                                <tr>
                                    <td>
                                    <label>Id_number:</label>
                                    </td>
                                    <td>
                                    <input name='id_no' type="text" value={id_no} onChange={(e)=>this.handleChange(e)} />
                                        <br/><small>{this.state.errorId_no}</small>
                                    </td>
                                    <td>
                                        
                                    </td>
                                </tr>
                                <tr>    
                                    <td>
                                    <label>Name:</label>
                                    </td>
                                    <td>
                                    <input name='name' type="text" value={name} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorName}</small>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label>Email:</label>
                                    </td>
                                    <td>
                                    <input name='email' type="text" value={email} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorEmail}</small>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label>Password:</label>
                                    </td>
                                    <td>
                                    <input name='password' type="password" value={password} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorPassword}</small>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                    <label>Confirm Password:</label>
                                    </td>
                                    <td>
                                    <input name='cnf_password' type="password" value={cnf_password} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorCnf_Password}</small>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td>
                                    <label>Phone:</label>
                                    </td>
                                    <td>
                                    <input name='phone' type="integer" value={phone} onChange={(e)=>this.handleChange(e)} />
                                    <br/><small>{this.state.errorPhone}</small>
                                    </td>
                                    <td>
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label>Gender:</label>
                                    </td>
                                    <td>
                                    <label><input type="radio" id="male" name="gender" value='male' onChange={(e)=>this.handleChange(e)}/>Male</label>
                                    <label><input type="radio" id="female" name="gender" value='female' onChange={(e)=>this.handleChange(e)}/>Female</label>
                                        <br/><small>{this.state.errorGender}</small>
                                    </td>
                                        <td>
                                        </td>
                                </tr>
                                <tr>
                                    <td>
                                    <label>Note:</label>
                                    </td>
                                    <td>
                                    <textarea name='note' value={note} onChange={(e)=>this.handleChange(e)}/>
                                    <br/><small>{this.state.errorNote}</small>
                                    
                                    </td>
                                    <td>
                                        
                                    
                                    </td>
                                </tr>
                                {/* <input type="file" onChange={this.handleChnage} /> */}
                                <tr>
                                    <td>
                                        Select year:
                                    </td>
                                    <td>
                                    <select name='opt' value={[opt]} onChange={(e)=>this.handleChange(e)} multiple>
                                    {/* <select multiple name='opt'value={opt} onChange={this.changeOpt}> */}
                                        <option hidden>-Select-</option>
                                        <option value='1'>One</option>
                                        <option value='2'>two</option>
                                        <option value='3'>three</option>
                                    </select>
                                    <br/><small>{this.state.errorOpt}</small>
                                    </td>
                                    <td>
                                    </td>
                                </tr>   
                                <tr>
                                    <td>
                                        Select year:
                                    </td>
                                    <td>
                                    <label><input type="checkbox" name="sports1" value="cycling" onChange={(e)=>this.handleChange(e)}/> cycling</label>
                                    <label><input type="checkbox" name="sports2" value="running" onChange={(e)=>this.handleChange(e)}/> running</label>
                                    <label><input type="checkbox" name="sports3" value="visit gym" onChange={(e)=>this.handleChange(e)}/> visit gym</label>
                                        <br/><small>{this.state.errorSports}</small>
                                    </td>
                                    {/* <td>
                                        <label>
                                            <Checkbox checked={this.state.checked} onChange={this.handleCheckboxChange}>

                                            </Checkbox><span>Label Text</span></label>

                                    </td> */}
                                </tr>       
                                <tr>
                                    <td colSpan='2'><input type="submit" value="Submit"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
                
            </div>
            
        )
    }
}

export default ReactForm
