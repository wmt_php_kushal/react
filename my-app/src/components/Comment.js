import React from 'react'
import ReactDOM from 'react-dom'
function formatDate(date) {
    return date.toLocaleDateString();
  }
  function Comment( props) {
    return (
            <div className='Comment'>
                <div className="userInfo">
                    <img className='avtar' src={props.author.avtarurl}/>
                    <div className='UserNameInfo-name'>{props.author.name}</div>
                    <div className='Comment-text'>{props.text}</div>
                    <div className='Comment-date'>{formatDate(props.date)}</div>
                </div>
            </div>
            );
        }
    
    const comment={
        date:new Date(),
        text:'some Text',
        author:{
            name:'Hello',
            avtarurl:'https://placekitten.com/g/250/250',
        },
    };
    ReactDOM.render(<Comment date={comment.date} text={comment.text} author={comment.author}></Comment>,document.getElementById('root'))

export default Comment
